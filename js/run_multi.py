import random
import os

# Insert your bot at position 0
BOTS=[ '"node MyBot.js"',
       '"python ../tools/sample_bots/python/LeftyBot.py"', 
       '"python ../tools/sample_bots/python/HunterBot.py"',
       '"python ../tools/sample_bots/python/GreedyBot.py"' ]
# Add more maps to the rotation; should be map location/the number of players pairs
MAPS=[ '../tools/maps/maze/maze_02p_01.map', 2, 
       '../tools/maps/multi_hill_maze/maze_04p_01.map', 4, 
       '../tools/maps/random_walk/random_walk_08p_01.map', 8 ]

TURN_TIMEOUT=500

# Change this to how many times you want it to run
MAX_RUNS=5
NUM_MAPS=len(MAPS) / 2

NUM_BOTS=len(BOTS)

for i in range(0, MAX_RUNS):
    map_index=random.randint(0, NUM_MAPS - 1) * 2 
    num_players=MAPS[map_index + 1]
    map=MAPS[map_index]

    players=BOTS[0] # Always add your bot at least once
    for p in range(1, num_players):
        rand_p=random.randint(0, NUM_BOTS - 1)
        players=players + " " + BOTS[rand_p]
    
    cmd="../tools/playgame.py " + players + " --map_file " + map + " --log_dir game_logs" + str(i) + " --turns 1000 --player_seed 7 --verbose -e -E --turntime " + str(TURN_TIMEOUT)
    print(cmd)
    os.system('python ' + cmd)

