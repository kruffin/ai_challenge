var fs = require('fs')
var Defines = require('./Defines')

exports.ants = {
	'bot': null,
	'currentTurn': -1,
    'turnStartTime': 0,
	'config': {},
	'orders': [],
    'timeRemaining': function() {
        return this.config.turntime - (new Date().getTime() - this.turnStartTime);
    },
	'start': function(botInput) {
		this.bot = botInput;
		process.stdin.resume();
		process.stdin.setEncoding('utf8');
		var thisoutside = this;
        var partialline = "";
		process.stdin.on('data', function(chunk) {
			var lines = chunk.split("\n");
            lines[0] = partialline + lines[0];
            partialline = "";
            if (lines[lines.length - 1] !== "") {
                partialline = lines[lines.length - 1];
                lines.splice(lines.length - 1, 1);
            }

			for (var i = 0, len = lines.length; i < len; ++i) {
				thisoutside.processLine(lines[i]);
			}
		});
	},
	'processLine': function(line) {
		line = line.trim().split(' ');

		if (line[0] === 'ready') {
			this.bot.onReady();
			return;
		} else if(line[0] === 'go') {
            if (Defines.DEBUG) {
                console.error("Turn: " + this.currentTurn + "|GO received.");
            }
			this.bot.onTurn();
			return;
		} else if(line[0] === 'end') {
			this.bot.onEnd();
			return;
		}
		if (line[0] === 'turn') {
            this.turnStartTime = new Date().getTime();
			this.currentTurn = parseInt(line[1]);
            this.bot.fogMap.setCurrentTurn(this.currentTurn);
            this.bot.fogMap.clearLastMoves();
            this.bot.fogMap.clearFood();
            this.antsOnHills = [];
            if (Defines.DEBUG) {
               console.error("Turn: " + this.currentTurn + "|TURN received|Hills cleared.");
            }
            return;
		} else {
			if (this.currentTurn === 0 && line[0] !== 'ready') {
				this.config[line[0]] = line[1];
			} else {
				var row = parseInt(line[1]);
				var col = parseInt(line[2]);

				if (line[0] === 'w') {
                    this.bot.fogMap.makeNotPassable(row, col);
				} else if (line[0] === 'f') {
                    this.bot.fogMap.addFood(row, col);
				} else {
					var owner = parseInt(line[3]);

					if (line[0] === 'a') {
                        if (owner === 0) {
                            this.bot.fogMap.addAnt(row, col);
                        }
                        // TODO: Something for enemy ants?
					} else if (line[0] === 'd') {
                        // TODO: Something for dead ants?
					} else  if (line[0] === 'h') {
                        this.bot.fogMap.addHill(row, col, owner);
					}
				}
			}
		}
	},
	'issueOrder': function(row, col, direction) {
        //console.error("issueOrder|row: " + row + "|col: " + col + "|dir: " + direction);
		this.orders.push({
			'row': parseInt(row),
			'col': parseInt(col),
			'direction': direction
		});
	},
	'finishTurn': function() {
		for (var i = 0; i < this.orders.length; i++) {
			var order = this.orders[i];
            var oldAnt = this.bot.fogMap._map[order.row][order.col].ant;
            if (oldAnt && !oldAnt.movedThisTurn) { continue; }

            fs.writeSync(process.stdout.fd, 'o '+order.row+' '+order.col+' '+order.direction+'\n');
		}
		this.orders = [];
        fs.writeSync(process.stdout.fd, 'go\n');
        process.stdout.flush();
        //fs.fsyncSync(process.stdout.fd);
	},
	'tileInDirection': function(row, col, direction) {
		var rowd = 0;
		var cold = 0;
		if (direction === 'N') {
			rowd = -1;
		} else if (direction === 'E') {
			cold = 1;
		} else if (direction === 'S') {
			rowd = 1;
		} else if (direction === 'W') {
			cold = -1;
		}
		var newrow = row + rowd;
		var newcol = col + cold;
		if (newrow < 0) {
			newrow = this.config.rows-1;
		} else if (newrow > this.config.rows-1) {
			newrow = 0;
		}
		if (newcol < 0) {
			newcol = this.config.cols-1;
		} else if (newcol > this.config.cols-1) {
			newcol = 0;
		}
		return this.map[newrow][newcol];
	},
	'destination': function(row, col, direction) {
		var rowd = 0;
		var cold = 0;
		if (direction === 'N') {
			rowd = -1;
		} else if (direction === 'E') {
			cold = 1;
		} else if (direction === 'S') {
			rowd = 1;
		} else if (direction === 'W') {
			cold = -1;
		}
		var newrow = row + rowd;
		var newcol = col + cold;
		if (newrow < 0) {
			newrow = this.config.rows-1;
		} else if (newrow > this.config.rows-1) {
			newrow = 0;
		}
		if (newcol < 0) {
			newcol = this.config.cols-1;
		} else if (newcol > this.config.cols-1) {
			newcol = 0;
		}
		return [newrow, newcol];
	},
	'distance2': function(fromRow, fromCol, toRow, toCol) {
		var dr = Math.min(Math.abs(fromRow - toRow), this.config.rows - Math.abs(fromRow - toRow));
		var dc = Math.min(Math.abs(fromCol - toCol), this.config.cols - Math.abs(fromCol - toCol));
		return (dr * dr) + (dc * dc);
	},
    'distance': function(fromRow, fromCol, toRow, toCol) {
        return Math.sqrt(this.distance2(fromRow, fromCol, toRow, toCol));
    },
	'direction': function(fromRow, fromCol, toRow, toCol) {
		var d = [];
		fromRow = fromRow % this.config.rows;
		toRow = toRow % this.config.rows;
		fromCol = fromCol % this.config.cols;
		toCol = toCol % this.config.cols;
		
		if (fromRow < toRow) {
			if (toRow - fromRow >= this.config.rows/2) {
				d.push('N');
			}
			if (toRow - fromRow <= this.config.rows/2) {
				d.push('S');
			}
		} else if (toRow < fromRow) {
			if (fromRow - toRow >= this.config.rows/2) {
				d.push('S');
			}
			if (fromRow - toRow <= this.config.rows/2) {
				d.push('N');
			}
		}
		
		if (fromCol < toCol) {
			if (toCol - fromCol >= this.config.cols/2) {
				d.push('W');
			}
			if (toCol - fromCol <= this.config.cols/2) {
				d.push('E');
			}
		} else if (toCol < fromCol) {
			if (fromCol - toCol >= this.config.cols/2) {
				d.push('E');
			}
			if (fromCol - toCol <= this.config.cols/2) {
				d.push('W');
			}
		}
		return d;
	}
};
