
SortedHash = function(maxRows) {
    this._maxRows = maxRows;
    this._items = {};

    this.hasElements = function() {
        for (var k in this) {
            if (this.hasOwnProperty(k) && this[k] instanceof Array) {
                return true;
            }
        }
        return false;
    }

    this.hasItem = function(item) {
        return this._items[item.row + this._maxRows * item.col] !== undefined;
    }

    this.removeItem = function(item) {
        var obj = this._items[item.row + this._maxRows * item.col];
        if (obj) { 
            delete this._items[item.row + this._maxRows * item.col];
            var arr = this[obj.key];
            arr.splice(arr.indexOf(obj.value), 1);
            if (arr.length === 0) {
                delete this[obj.key];
            }

            return obj.value;
        } else {
            return undefined;
        }
    }


    this.add = function(key, value) {
        if (!this[key]) {
           this[key] = [ value ];
        } else {
            this[key].push(value);
        }

        this._items[value.row + this._maxRows * value.col] = { value: value, key: key };
    }

    this.removeTop = function() {
        for (var k in this) {
            if (this.hasOwnProperty(k) && this[k] instanceof Array) {
                var arr = this[k];
                var obj = arr.pop();
                if (arr.length === 0) {
                    delete this[k];
                }

                delete this._items[obj.row + this._maxRows * obj.col];

                return obj;
            }
        }

        return undefined;
    }
}

