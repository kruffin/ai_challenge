var Defines = require('./Defines')
require('./SortedHash')

AStar = function(map, maxRows, maxCols, timeoutFunc) {

    this._map = map;
    this._maxRows = maxRows;
    this._maxCols = maxCols;
    this._hasTimedOut = timeoutFunc;

    this.plotCourse = function(curRow, curCol, destRow, destCol) {
        var inspect = new SortedHash(this._maxRows);
        var closed = [];

        // Check early to see if this destination is possible
        if (!this._isReachable(destRow, destCol)) { return { path: [], destRow: destRow, destCol: destCol }; }

        // Set up the first element
        var obj = this._map[curRow][curCol];
        obj.parentNode = undefined;
        obj.G = 0;
        obj.H = this._manhattan(curRow, curCol, destRow, destCol);
        obj.F = obj.G + obj.H;
        inspect.add(obj.F, this._map[curRow][curCol]);

        var pathInfo = { startRow: curRow, startCol: curCol,
                         destRow: destRow, destCol: destCol,
                         closed: [], inspect: inspect };

        return this.plotCourseContinuation(pathInfo);

    }

    this.plotCourseContinuation = function(pathInfo) {
        var inspect = pathInfo.inspect;
        var closed = pathInfo.closed;
        var curRow = pathInfo.startRow;
        var curCol = pathInfo.startCol;
        var destRow = pathInfo.destRow;
        var destCol = pathInfo.destCol;
    
        while(inspect.hasElements()) {
            if (this._hasTimedOut()) { 
                return { startRow: curRow, startCol: curCol,
                         destRow: destRow, destCol: destCol,
                         closed: closed, inspect: inspect 
                       };
            }

            obj = inspect.removeTop();
            closed.push(obj);

            if (obj.row === destRow && obj.col === destCol) {
                // We found the target; return the path
                var path = [];
                var cur = obj;

                while (cur.row !== curRow || cur.col !== curCol) {
                    path.unshift(cur.dir);
                    cur = cur.parentNode;
                }

                return { path: path, destRow: destRow, destCol: destCol };
            }

            // Check the four directions
            this._checkDirection(obj, obj.row, obj.col + 1, Defines.EAST, destRow, destCol, closed, inspect);
            this._checkDirection(obj, obj.row, obj.col - 1, Defines.WEST, destRow, destCol, closed, inspect);
            this._checkDirection(obj, obj.row + 1, obj.col, Defines.SOUTH, destRow, destCol, closed, inspect);
            this._checkDirection(obj, obj.row - 1, obj.col, Defines.NORTH, destRow, destCol, closed, inspect);
        }

        return { path: [], destRow: destRow, destCol: destCol };
    }

    this._checkDirection = function(obj, row, col, dir, destRow, destCol, closed, inspect) {
        var east = this._getLocation(row, col);
        
        east = this._map[east.row][east.col];
        if (closed.indexOf(east) === -1) {
            if (!east.isPassable || (east.ant && east.ant.isDefender)) {
                closed.push(east);
                return;
            }
                
            var g = 1 + obj.G;
            var h = this._manhattan(east.row, east.col, destRow, destCol);
            var f = g + h;
            if (!inspect.hasItem(east)) {
                east.G = g;
                east.H = h;
                east.F = f;
                east.parentNode = obj;
                east.dir = dir;

                inspect.add(east.F, east);
                return;
            } else if (east.F > f) {
                inspect.removeItem(east);
                // It's on the inspect list already, but this is a better path
                east.G = g;
                east.H = h;
                east.F = f;
                east.parentNode = obj;
                east.dir = dir;
                inspect.add(east.F, east);
                return;
            }
        }
    }

    this._isReachable = function(row, col) {
        var obj = this._map[row][col];
        if (!obj.isPassable || (!this._getLocation(row, col, Defines.NORTH).isPassable &&
                !this._getLocation(row, col, Defines.SOUTH).isPassable &&
                !this._getLocation(row, col, Defines.EAST).isPassable &&
                !this._getLocation(row, col, Defines.WEST).isPassable)) { 
            return false; 
        }
        
        return true;
    }

    this._manhattan = function(startRow, startCol, endRow, endCol) {
        return Math.min(Math.abs(startRow - endRow), this._maxRows - Math.abs(startRow - endRow)) + 
               Math.min(Math.abs(startCol - endCol), this._maxCols - Math.abs(startCol - endCol));
    }

    /**
     * Gets the location (with wrapping) in the {dir} direction from {pos} 
     * position on the {_map}.
     * @param {row} The row to start at.
     * @param {col} The col to start at.
     * @param {dir} The direction to from {pos} to move in.
     * @return The location from {pos} in direction {dir}.
     */
    this._getLocation = function(row, col, dir) {
        switch (dir) {
            case Defines.NORTH:
                row -= 1;
                if (row < 0) { row = this._maxRows - 1; }
                break;
            case Defines.SOUTH:
                row += 1;
                if (row >= this._maxRows) { row = 0; }
                break;
            case Defines.WEST:
                col -= 1;
                if (col < 0) { col = this._maxCols - 1; }
                break;
            case Defines.EAST:
                col += 1;
                if (col >= this._maxCols) { col = 0; }
                break;
            default:
                if (row < 0) { row = this._maxRows - 1; }
                else if (row >= this._maxRows) { row = 0; }
                if (col < 0) { col = this._maxCols - 1; }
                else if (col >= this._maxCols) { col = 0; }
                break;
        }

        return this._map[row][col];
    }

}

Test_AStar = function() {
    

    var map = [];
    for (var r = 0; r < 200; r++) {
        map.push([]);
        for (var c = 0; c < 200; c++) {
            var passable = (r !== 5 || c !== 4);
            map[r][c] = { "row": r, "col": c, "isPassable": passable, "type": Defines.VIEWED };
        }
    }

    var astar = new AStar(map, 200, 200, function() { return false; });

    var start = map[0][0];

    var startTime = new Date().getTime();
    for (var r = 0; r < 200; r++) {
        var rowStartTime = new Date().getTime();
        for (var c = 0; c < 200; c++) {
            var pos = map[r][c];
            var plotStartTime = new Date().getTime();
            var path = astar.plotCourse(start.row, start.col, pos.row, pos.col);
            console.error("Plot Time: " + (new Date().getTime() - plotStartTime) + "ms| (" + start.row + ", " + start.col + ") to (" + pos.row + ", " + pos.col + ") " + JSON.stringify(path));
        }
        console.error("Row: " + r + " is done: " + (new Date().getTime() - rowStartTime) + "ms");
    }
    console.error("Total Time: " + ((new Date().getTime()) - startTime) + "ms");
}

//Test_AStar();
