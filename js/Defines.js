
exports.NORTH = 'N';
exports.SOUTH = 'S';
exports.EAST  = 'E';
exports.WEST  = 'W';
exports.HOLD  = 'H';

exports.DEBUG = false;

exports.TIMEOUT = 100;


exports.DENSE_FOG = 'Dense Fog';
exports.LIGHT_FOG = 'Light Fog';
exports.VIEWED = 'Viewed';
