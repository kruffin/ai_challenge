Cell = function(topLeft, width, height) {

    this._topLeftPoint = topLeft;
    this._width = width;
    this._height = height;

    this._halfWidth = Math.floor(this._width / 2);
    this._halfHeight = Math.floor(this._height / 2);

    this._myAnts = [];
    this._enemyAnts = [];

    this.addEnemyAnt = function(row, col) {
        this._enemyAnts.push( { row: row, col: col } );
    }

    this.addMyAnt = function(row, col) {
        this._myAnts.push( { row: row, col: col } );
    }
}

Grid = function(topLeft, width, height, parent) {
    this.MIN = 5;

    this._topLeft = topLeft;
    this._width = width;
    this._height = height;
    this._parent = parent;
    this._one;
    this._two;
    this._three;
    this._four;
    this._cell;

    if (width <= this.MIN || height <= this.MIN) {
        this._cell = new Cell(topLeft, width, height);
    } else {
        // Create four subparts
        var halfWidth = width / 2;
        var halfHeight = height / 2;

        this._one = new Grid(topLeft, halfWidth, halfHeight, this);
        this._two = new Grid( { row: topLeft.row + halfHeight, col: topLeft.col }, halfWidth, halfHeight, this);
        this._three = new Grid ( { row: topLeft.row, col: topLeft.col + halfWidth }, halfWidth, halfHeight, this);
        this._four = new Grid( {row: topLeft.row + halfHeight, col: topLeft.col + halfWidth }, halfWidth, halfHeight, this);
    }

    this.getContainingGrid = function(pos) {
        if (pos.row >= this._topLeft.row && pos.row < this._topLeft.row + this._width &&
                pos.col >= this._topLeft.col && pos.col < this._topLeft.col + this._height) {
            // Its within this grid; check the children
            if (!this._one) {
                return this;
            }

            var grid = this._one.getContainingGrid(pos);
            if (grid) { return grid; }
            grid = this._two.getContainingGrid(pos);
            if (grid) { return grid; }
            grid = this._three.getContainingGrid(pos);
            if (grid) { return grid; }
            grid = this._four.getContainingGrid(pos);
            if (grid) { return grid; }
        }

        return undefined;
    }
}
