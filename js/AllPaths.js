require('./Defines');

AllPaths = function(maxRows, maxCols) {
    this._maxRows = maxRows;
    this._maxCols = maxCols;
    this._bitfield = new BitField();

    this.NORTH_BITS = 0x0;
    this.SOUTH_BITS = 0x1;
    this.WEST_BITS = 0x2;
    this.EAST_BITS = 0x3;

    this.getDirection = function(start, end) {
        
    }
}


/**
 * Code taken from: http://stackoverflow.com/questions/3435693/create-a-large-bit-field
 */
BitField = function() {
    this.values = [];

    this.get = function(i) {
        var index = (i / 32) | 0; // | 0 converts to an int. Math.floor works too.
        var bit = i % 32;
        return (this.values[index] & (1 << bit)) !== 0;
    }

    this.set = function(i) {
        var index = (i / 32) | 0;
        var bit = i % 32;
        this.values[index] |= 1 << bit;    
    }

    this.unset = function(i) {
        var index = (i / 32) | 0;
        var bit = i % 32;
        this.values[index] &= ~(1 << bit);    
    }
}
        
