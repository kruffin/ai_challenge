var Defines = require('./Defines');

Map = function() {
    this._maxRows = 0;
    this._maxCols = 0;

    this._map = [];

    // Lists of significant items on the map
    this._ants = [];
    this._enemyAnts = [];
    this._hills = [];
    this._enemyHills = [];
    this._denseFog = [];
    this._lightFog = [];
    this._food = [];
    this._impassable = [];
    this._passable = [];

    // Useful Defines
    this._sightRadius2;
    this._sightRadius;
    this._currentTurn;

    // Temporary Items
    this._antMoves = [];

    // Static defines
    this._DEBUG = false;

    this.DENSE_FOG = 'Dense Fog';
    this.LIGHT_FOG = 'Light Fog';
    this.VIEWED = 'Viewed';

    this.NORTH = 'N';
    this.SOUTH = 'S';
    this.EAST  = 'E';
    this.WEST  = 'W';

    /**
     * Sets up the map with the input maximum bounds.
     * @param {rows} the maximum rows in the map
     * @param {cols} the maximum columns in the map
     * @param {sightR2} the squared sight radius of the viewer (360 degree view assumed)
     */
    this.setup = function(rows, cols, sightR2) {
        if (this.DEBUG) {
            console.error("Map dimensions: [" + rows + ", " + cols + "]");
        }
        this._maxRows = rows;
        this._maxCols = cols;
        this._sightRadius2 = sightR2;
        this._sightRadius = Math.ceil(Math.sqrt(this._sightRadius2));

        for (var r = 0; r < rows; r++) {
            this._map[r] = [];
            for (var c = 0; c < cols; c++) {
                this._map[r][c] = { type: this.DENSE_FOG, row: r, col: c, isPassable: true };
                this._denseFog.push(this._map[r][c]);
                this._passable.push(this._map[r][c]);
            }
        }
    }

    this.clearLastMoves = function() {
        this._antMoves = [];
        for (var i = 0; i < this._ants.length; i++) {
            this._ants[i].hasMove = undefined;
        }
    }

    this.setCurrentTurn = function(curTurn) {
        this._currentTurn = curTurn;
    }

    this._removeDeadAnt = function(ant) {
        this._ants.splice(this._ants.indexOf(ant), 1);
        this._map[ant.row][ant.col].ant = undefined;
    }

    this.makeNotPassable = function(row, col) {
        var obj = this._map[row][col];
        obj.isPassable = false;
        this._passable.splice(this._passable.indexOf(obj), 1);
        this._impassable.push(obj);
    }

    this.clearFood = function() {
        this._food = [];
    }

    this.addFood = function(row, col) {
        this._food.push(this._map[row][col]);
    }

    this.addHill = function(row, col, owner) {
        var obj = this._map[row][col];

        if (owner === 0 && this._hills.indexOf(obj) === -1) {
            if (this.DEBUG) {
                console.error("Adding hill at (" + row + ", " + col + ")");
            }
            this._hills.push(obj);
            this.makeNotPassable(row, col);
        } else if (owner !== 0 && this._enemyHills.indexOf(obj) === -1) {
            if (this.DEBUG) {
                console.error("Adding enemy hill at (" + row + ", " + col + ")");
            }
            obj.isEnemyHill = true;
            this._enemyHills.push(obj);
        }
    }

    this.removeHillj = function(row, col) {
        var obj = this._map[row][col];
        this._enemyHills.splice(this._enemyHills.indexOf(obj), 1);
        obj.isEnemyHill = undefined;
    }

    this.addAnt = function(row, col) {
        if (this.DEBUG) {
            console.error("Turn: " + this._currentTurn + "|Ant at (" + row + ", " + col + ")");
        }
        var locs = [];

        var tile = this.getTileInDirection(row, col, this.NORTH);
        if (tile.ant && tile.ant.path.length > 0 && tile.ant.path[0] === this.SOUTH) {
            locs.push(tile.ant);
        }
        tile = this.getTileInDirection(row, col, this.SOUTH);
        if (tile.ant && tile.ant.path.length > 0 && tile.ant.path[0] === this.NORTH) {
            locs.push(tile.ant);
        }
        tile = this.getTileInDirection(row, col, this.WEST);
        if (tile.ant && tile.ant.path.length > 0 && tile.ant.path[0] === this.EAST) {
            locs.push(tile.ant);
        }
        tile = this.getTileInDirection(row, col, this.EAST);
        if (tile.ant && tile.ant.path.length > 0 && tile.ant.path[0] === this.WEST) {
            locs.push(tile.ant);
        }
        tile = this._map[row][col];
        if (tile.ant) {
            // May not have moved; it's important that this position be at the end
            locs.push(tile.ant);
        } else {
            // This may be a new ant
            //locs.push( { row: row, col: col, path: [], visited: [], curTurn: this._currentTurn } );
        }

        // If an ant is on an enemy ant hill remove it
        var hillIndex = this._enemyHills.indexOf(tile);
        if (hillIndex !== -1) {
            this._enemyHills.splice(hillIndex, 1);
        }

        if (!this._antMoves[locs.length]) {
            this._antMoves[locs.length] = [];
        }
        this._antMoves[locs.length].push( { row: row, col: col, locations: locs } );
    }

    /**
     * This method should be called after all ants for the turn have been added via
     * the addAnt(row, col) method. It will match ants from the last turn with ants
     * this turn.
     */
    this.assignMoves = function() {
        if (this.DEBUG) {
            console.error("Turn: " + this._currentTurn + "|Ants to inspect: " + this._antMoves.length);
        }
        // Ants are now in buckets according to their length of possible locations (max of 5 buckets)
        // Look at bucket 0 first and go from there

        for (var b in this._antMoves) {
            for (var i = 0; i < this._antMoves[b].length; i++) {
                var move = this._antMoves[b][i];
                //console.error("Turn: " + this._currentTurn + "|Looking at possible move: " + JSON.stringify(move));
                for (var a = 0; a < move.locations.length; a++) {
                    var curAnt = move.locations[a];
                    if (!curAnt.hasMove) {
                        move.finished = true;
                        curAnt.hasMove = this._map[move.row][move.col];
                        break;
                    }
                }
            
                if (!move.finished) {
                    // This is a new ant!
                    var curAnt = this._addNewAnt(move.row, move.col);
                    curAnt.hasMove = this._map[move.row][move.col];
                }
            }
        }

        // Actually perform the moves
        var deadAnts = [];
        for (var ai = 0; ai < this._ants.length; ai++) {
            var curAnt = this._ants[ai];
            curAnt.movedThisTurn = false;

            if (!curAnt.hasMove) {
                deadAnts.push(curAnt);
                continue;
            }
            // This is the move we are looking for
            var oldTile = this._map[curAnt.row][curAnt.col];
            var newTile = curAnt.hasMove;

            curAnt.row = newTile.row;
            curAnt.col = newTile.col;
            oldTile.ant = undefined;
            newTile.ant = curAnt;

            // Increment its path
            if (curAnt.path.length > 0) {
                var dir = curAnt.path.shift();
                curAnt.visited.push(dir);
                if (this.DEBUG) {
                    console.error("Turn: " + this._currentTurn + "|Ant Moved from (" + oldTile.row + ", " + oldTile.col + ") to " + dir + "(" + newTile.row + ", " + newTile.col + ")");
                }
            }
        }

        // Remove dead
        for (var di = 0; di < deadAnts.length; di++) {
            this._removeDeadAnt(deadAnts[di]);
        }

        return deadAnts;
    }

    this.getTileInDirection = function(row, col, direction) {
        switch (direction) {
            case Defines.NORTH:
                row -= 1;
                if (row < 0) { row = this._maxRows - 1; }
                break;
            case Defines.SOUTH:
                row += 1;
                if (row >= this._maxRows) { row = 0; }
                break;
            case Defines.WEST:
                col -= 1;
                if (col < 0) { col = this._maxCols - 1; }
                break;
            case Defines.EAST:
                col += 1;
                if (col >= this._maxCols) { col = 0; }
                break;
        }

        return this._map[row][col];
	} 
    
    this._addNewAnt = function(row, col) {
        var obj = this._map[row][col];
        var newAnt = { row: row, col: col, path: [], visited: [], curTurn: this._currentTurn };
        this._ants.push(newAnt);
        // Add the ant to the map square
        obj.ant = newAnt;
        if (this.DEBUG) {
            console.error("Turn: " + this._currentTurn + "|New ant added at: (" + row + ", " + col + ")");
        }
        return newAnt;
    }

    this.distance2 = function(sRow, sCol, dRow, dCol) {
        var dr = Math.min(Math.abs(sRow - dRow), this._maxRows - Math.abs(sRow - dRow));
		var dc = Math.min(Math.abs(sCol - dCol), this._maxCols - Math.abs(sCol - dCol));
		return (dr * dr) + (dc * dc);
    }

    /**
     * Modifies the type of elements in the map; moves dense fog to light fog
     * and light fog to passable or impassable.
     * @param {posRow} the row position of the viewer
     * @param {posCol} the column position of the viewer
     */
    this.alterFog = function(posRow, posCol) {
        // Look in a local area
        for (var r = posRow - (this._sightRadius - 1); r <= posRow + (this._sightRadius - 1); r++) {
            for (var c = posCol - (this._sightRadius - 1); c <= posCol + (this._sightRadius - 1); c++) {

                var pos = this._wrapPosition(r, c);

                if (this._map[pos.row][pos.col].type !== this.DENSE_FOG && this._map[pos.row][pos.col].type !== this.LIGHT_FOG) {
                    continue;
                }
                
                // Cut out the corners
                if (this._map[pos.row][pos.col].type !== this.LIGHT_FOG && this.distance2(posRow, posCol, r, c) > this._sightRadius2) {
                    //this._alterType(pos.row, pos.col, this.LIGHT_FOG);
                    continue;
                }

                this._alterType(pos.row, pos.col, this.VIEWED);
            }  
        }

        this._createFogRing(posRow, posCol);
    }

    this._alterType = function(posRow, posCol, type) {
        var obj = this._map[posRow][posCol];
        if (type === this.VIEWED) {
            if (obj.type === this.DENSE_FOG) {
                obj.type = type;
                this._denseFog.splice(this._denseFog.indexOf(obj), 1);
            } else if (obj.type === this.LIGHT_FOG) {
                obj.type = type;
                this._lightFog.splice(this._lightFog.indexOf(obj), 1);
            }
        } else if (type === this.LIGHT_FOG) {
            if (obj.type === this.DENSE_FOG) {
                obj.type = type;
                this._denseFog.splice(this._denseFog.indexOf(obj), 1);
                this._lightFog.push(obj);
            }
        }
    }

    this._wrapPosition = function(row, col) {
        var rt = row;
        var ct = col;
        if (rt < 0) {
            rt += this._maxRows - 1;
        } else if (rt >= this._maxRows) {
            rt -= this._maxRows;
        }
        if (ct < 0) {
             ct += this._maxCols - 1;
        } else if (ct >= this._maxCols) {
             ct -= this._maxCols;
        }

        return { row: rt, col: ct };
    }

    this._createFogRing = function(posRow, posCol) {
        var thisthis = this;
        this.circle(posRow, posCol, this._sightRadius + 1, function(row, col) { thisthis._alterType(row, col, thisthis.LIGHT_FOG); });
    }

    // Bresenham's from http://willperone.net/Code/codecircle.php
    this.circle = function(crow, ccol, radius, func) {
        var x = 0;
        var y = radius;
        var p = 3 - 2 * radius;

        while (y >= x) {
            var pos = this._wrapPosition(Math.ceil(crow - x), Math.ceil(ccol - y));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow - y), Math.ceil(ccol - x));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow + y), Math.ceil(ccol - x));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow + x), Math.ceil(ccol - y));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow - x), Math.ceil(ccol + y));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow - y), Math.ceil(ccol + x));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow + y), Math.ceil(ccol + x));
            func(pos.row, pos.col);
            pos = this._wrapPosition(Math.ceil(crow + x), Math.ceil(ccol + y));
            func(pos.row, pos.col);

            if (p < 0) { 
                p += 4 * x++ + 6; 
            } else {
                p += 4 * (x++ - y--) + 10;
            }
        }
    }

}

Test_Circle = function() {
    var m = new Map();
    m.circle(0, 0, 3, function(row, col) { console.log("Pos: (" + row + ", " + col + ")"); });
}


Test_Map = function() {
    var map = new Map();
    map.setup(200, 200, 5);
    var start = new Date().getTime();
    var course = map.plotCourse(0, 0, 100, 100);
    console.error("Time: " + (new Date().getTime() - start) + "ms|Path from (0, 0) -> (100, 100) =" + JSON.stringify(course));
    start = new Date().getTime();
    course = map.plotCourse(2, 2, 5, 5);
    console.error("Time: " + (new Date().getTime() - start) + "ms|Path from (2, 2) -> (5, 5) =" + JSON.stringify(course));
}
