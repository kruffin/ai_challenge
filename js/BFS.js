var Defines = require('./Defines');

BFS = function() {

    this.MAX_DISTANCE = 11;
    
    /**
     * Finds paths from each of the {@startLocs} to a desired location in the
     * {@dll} (Desired Locations List) using the input {@map}.
     * @param {startLocs} A list of starting locations.
     * @param {map} The map to search through, each element is of the form
     *              {row, col, isPassable}.
     * @param {timeoutFunc} A method that returns {true} if the method has 
     *                      exceeded the timeout time and {false} if it has
     *                      not.
     * @return A list of {loc, path} where {loc} is a starting location from
     *         {@startLocs} and {path} is a list of directions from the start
     *         to a desired location in {@dll}.
     */
    this.findPaths = function(startLocs, map, dll, timeoutFunc) {
        var paths = this._initPaths(startLocs, dll, map);
        var directions =  [ Defines.NORTH, Defines.SOUTH, Defines.WEST, Defines.EAST ];
        
        while(paths.bfsl.length > 0) {
            if (timeoutFunc() || dll.length === 0) {
                break;
            }

            var cur = paths.bfsl.shift();
            if (!cur.startTime) { cur.startTime = new Date().getTime(); }

            //console.error("(" + cur.loc.row + ", " + cur.loc.col + ")|History Length: " + cur.history.length + "|Next Locations: " + cur.nextLocs.length);

            var nextLocs = [];
            var distance = 0;
            for (var i = 0; i < cur.nextLocs.length; i++) {
                var pos = cur.nextLocs[i];
                // Add this position to the visited if it is not already there
                cur.history.push(pos.loc);

                var index = dll.indexOf(pos.loc);
                if (index !== -1) {
                    // We found a desired location
                    paths.finished.push( { "loc": cur.loc, "dir": pos.dir, "totalTime": (new Date().getTime() - cur.startTime) } );
                    dll.splice(index, 1);
                    cur.isFinished = true;
                    break;
                } else {
                    // Add the locations around this one
                    for (var d = 0; d < directions.length; d++) {
                        //console.error("Pos: " + JSON.stringify(pos));
                        var posInDir = this._getLocation(map, pos.loc, directions[d]);
                        if (cur.history.indexOf(posInDir) === -1 && this._posInNextList(posInDir, nextLocs) === -1 && this._posInNextList(posInDir, cur.nextLocs) === -1 && posInDir.isPassable && posInDir.type !== Defines.DENSE_FOG) {
                            // We haven't been here
                            dirs = pos.dir.slice(0);
                            dirs.push(directions[d]);
                            nextLocs.push( { "loc": posInDir, "dir": dirs } ); 
                            if (dirs.length > distance) {
                                distance = dirs.length;
                            }
                        }
                    }
                }
            }

            cur.nextLocs = nextLocs;

            // Check if a path was found
            if (!cur.isFinished && cur.nextLocs.length > 0) { //&& distance < this.MAX_DISTANCE) {
                paths.bfsl.push(cur);
            }
        }

        return paths.finished;
    }

    this._posInList = function(pos, arr) {
        for (var i = 0; i < arr.length; i++) {
            
            if (arr[i].row === pos.row && arr[i].col === pos.col) {
                return i;
            }
        }
        
        return -1;
    }

    this._posInNextList = function(pos, arr) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].loc.row === pos.row && arr[i].loc.col === pos.col) {
                return i;
            }
        }
        
        return -1;
    }

    /**
     * Gets the location (with wrapping) in the {dir} direction from {pos} 
     * position on the {map}.
     * @param {map} The map to use.
     * @param {pos} The position to start at.
     * @param {dir} The direction to from {pos} to move in.
     * @return The location from {pos} in direction {dir}.
     */
    this._getLocation = function(map, pos, dir) {
        //console.error("Pos: (" + pos.row + ", " + pos.col + ")");
        var maxRows = map.length;
        var maxCols = map[0].length;
        var row = pos.row;
        var col = pos.col;

        switch (dir) {
            case Defines.NORTH:
                row -= 1;
                if (row < 0) { row = maxRows - 1; }
                break;
            case Defines.SOUTH:
                row += 1;
                if (row >= maxRows) { row = 0; }
                break;
            case Defines.WEST:
                col -= 1;
                if (col < 0) { col = maxCols - 1; }
                break;
            case Defines.EAST:
                col += 1;
                if (col >= maxCols) { col = 0; }
                break;
        }

        return map[row][col];
    }

    /**
     * Initializes the Breadth First Search List {bfsl} with a location and a
     * direction array. If any of the {@startLocs} are on a desired location,
     * then it is added to the {finished} list.
     * @param {startLocs} The starting locations of the form {row, col}.
     * @param {dll} The desired locations list of the form {row, col}.
     * @param {map} The map
     * @return An object of the form {bfsl, finished, history} where {bfsl} is 
     *         an array of {loc: {row, col}, nextLocs: [], history: []} where 
     *         {nextLocs} is an array of {loc: {row, col}, dir: []} objects and
     *         {history} is an array of visited locations and {finished} is 
     *         also an array of {loc: {row, col}, dir: []} objects that have 
     *         reached a {dll}.
     */
    this._initPaths = function(startLocs, dll, map) {
        var bfsl = [];
        var finished = [];

        for (var i = 0; i < startLocs.length; i++) {
            var cloc = startLocs[i];
            var index = dll.indexOf(cloc);

            if (index !== -1) {
                // It's on a desired location; keep it there
                finished.push( { "loc": cloc, "dir": [ Defines.HOLD ] } );
                // Remove the location from the desired locations list
                dll.splice(index, 1);
            } else {
                bfsl.push( { "loc": cloc, "nextLocs": [ { "loc": map[cloc.row][cloc.col], "dir": [] } ], "history": [] } );
            }
        }

        return { "bfsl": bfsl, "finished": finished }
    }
}

Test_BFS = function() {

    var map = [];
    for (var r = 0; r < 200; r++) {
        map.push([]);
        for (var c = 0; c < 200; c++) {
            var passable = (r !== 5 || c !== 4);
            map[r][c] = { "row": r, "col": c, "isPassable": passable, "type": Defines.VIEWED };
        }
    }

    //console.log(JSON.stringify(map));
    var startLocs = [ map[0][0], map[2][2] ];
    var dll = [ map[100][100], map[5][5] ];

    var bfs = new BFS();
    var paths = bfs.findPaths(startLocs, map, dll, function() { return false; });
    console.log(JSON.stringify(paths));
}

