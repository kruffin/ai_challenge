var ants = require('./Ants').ants;
require('./Map');
require('./AStar');
var Defines = require('./Defines');

Bot = function() {
    this.targets = [];
    this.orders = {};
    this.boxes = [];
    this.fogMap = new Map();
    this.astar = {};

    this.onReady = function() {
        this.fogMap.setup(ants.config.rows, ants.config.cols, ants.config.viewradius2);
        this.astar = new AStar(this.fogMap._map, ants.config.rows, ants.config.cols, this.hasTimedOut);
        ants.finishTurn();
    }

    this.moveInDirection = function(ant, dir) {
        if (ant.movedThisTurn) { return false; }
        var obj = this.fogMap.getTileInDirection(ant.row, ant.col, dir);

        if (!obj.isPassable) {
            this.targets.splice(this.targets.indexOf(ant.path.target), 1);
            ant.path = [];

            return false;
        } else if (!this.orders[obj.row + this.fogMap._maxRows * obj.col] && (!obj.ant)) { //|| (obj.ant && obj.ant.path.length > 0))) {
            ants.issueOrder(ant.row, ant.col, dir);
            ant.movedThisTurn = true;
            this.orders[obj.row + this.fogMap._maxRows * obj.col] = true;
            return true;
        }

        if (this.fogMap.DEBUG) {
            console.error("Couldn't move ant (" + ant.row + ", " + ant.col + ") -> " + dir);
        }
        return false;
    }

    this.moveToLocation = function(ant, dest) {
        var start = new Date().getTime();
        var pathInfo = this.astar.plotCourse(ant.row, ant.col, dest.row, dest.col);
        if (this.fogMap.DEBUG) {
            var end = new Date().getTime();
            console.error("Turn: " +  this.fogMap._currentTurn + "|" + (end - start) + "ms|Path from (" + ant.row + ", " + ant.col + ") to (" + dest.row + ", " + dest.col + "): " + JSON.stringify(path));
        }

        this._continueAntPathing(ant, pathInfo);

/*
        if (path.path && path.path.length > 0) {
            if (ant.path) {
                // Remove it's last target
                this.targets.splice(this.targets.indexOf(ant.path.target), 1);
            }
            this.targets.push(dest);
            ant.path = path.path;
            ant.path.target = dest;
            ant.visited = [];
        } else if (!path.path) {
            // Couldn't finish; took too long
            path.ant = ant;
            this.pathFindingStore = path;
        }*/
    }

    this._passOrders = function(orders, row, col) {
        if (orders.length > 0) {this.u
            var ant = this.fogMap._map[row][col].ant;
            if (ant && ant.path.length === 0) {
                ant.path.unshift(orders.shift());
                var loc = this.fogMap.getTileInDirection(row, col, ant.path[0]);
                var o = this._passOrders(orders, loc.row, loc.col);
                if (o.length > 0) {
                    // Couldn't pass them
                    orders.unshift(ant.path[0]);
                    ant.path = orders;
                }

                return [];
            } else {
                return orders;
            }
        } else {
            // Nothing else to do
            return [];
        }
    }

    this.moveAnts = function() {
        var myAnts = this.fogMap._ants;

        for(var i = 0; i < myAnts.length; i++) {
            if (ants.timeRemaining() < Defines.TIMEOUT) {
                if (this.fogMap.DEBUG) {
                    console.error("Turn: " + this.fogMap._currentTurn + "|Took too long, had to break out moving ants.");
                }
                return false;
            }

            var a = myAnts[i];
            if (a.path.length > 0) {
                this.moveInDirection(a, a.path[0]);
            }
        }

        return true;
    }

    this.cleanUpAfterDeadAnts = function(deadAnts) {
        for (var di = 0; di < deadAnts.length; di++) {
            if (deadAnts[di].path) {
                this.targets.splice(this.targets.indexOf(deadAnts[di].path.target), 1);
            }
        }
    }

    this.hasTimedOut = function() {
        return ants.timeRemaining() < Defines.TIMEOUT;
    }

    this._getClosest = function(ant, locs, closest) {
        for (var i = 0; i < locs.length; i++) {
            var cur = locs[i];
            var dist = this.fogMap.distance2(ant.row, ant.col, cur.row, cur.col);
            if (!closest || dist < closest.dist) {
                closest = { dist: dist, array: locs, index: i, item: cur };
            }
        }

        return closest;
    }

    this.getGoal = function(ant, food, formation, unseen) {
        var hills = this.fogMap._enemyHills;

        var r = Math.floor(Math.random() * 101);

        var closest;
        //closest = this._getClosest(ant, formation, closest);

        if (r <= 25 && hills.length > 0) {
            // Attack a hill
            closest = this._getClosest(ant, hills, closest);
        } else {
            // Gather food
            closest = this._getClosest(ant, food, closest);

            // Explore
            closest = this._getClosest(ant, unseen, closest);
        }

        if (closest) {
            var item = closest.item;
            closest.array.splice(closest.index, 1);
            return item;
        }

        return undefined;
    }

    this._continueAntPathing = function(ant, pathInfo) {
        var path = pathInfo.path;
        var dest = this.fogMap._map[pathInfo.destRow][pathInfo.destCol];
        
        if (path && path.length > 0) {
            delete this.pathFindingStore;

            if (ant.path) {
                // Remove it's last target
                this.targets.splice(this.targets.indexOf(ant.path.target), 1);
            }
            this.targets.push(dest);
            ant.path = path;
            ant.path.target = dest;
            ant.visited = [];
        } else if (!path) {
            // Still didn't finish
            pathInfo.ant = ant;
            this.pathFindingStore = pathInfo;
        } else {
            delete this.pathFindingStore;
        }
    }

    this.onTurn = function() {
        this.orders = {};

        var startTime = new Date().getTime();
        this.cleanUpAfterDeadAnts(this.fogMap.assignMoves());
        
        if (Defines.DEBUG) {
            console.error("Turn: " + this.fogMap._currentTurn + "|onTurn Start|Total Ants: " + this.fogMap._ants.length);
        }

        for (var ai = 0; ai < this.fogMap._ants.length; ai++) {
            if (this.hasTimedOut()) {
                ants.finishTurn();
                if (Defines.DEBUG) {
                    console.error("Turn: " + this.fogMap._currentTurn + "|Took too long, had to break out altering fog.");
                }
                return;
            }
            var ant = this.fogMap._ants[ai];
            this.fogMap.alterFog(this.fogMap._ants[ai].row, this.fogMap._ants[ai].col);
        }
        
        if (Defines.DEBUG) {
            console.error("Prep time: " + (new Date().getTime() - startTime) + "ms");
        }

        startTime = new Date().getTime();
        this.moveAnts();
        if (Defines.DEBUG) {
            console.error("First Move time: " + (new Date().getTime() - startTime) + "ms");
        }

        startTime = new Date().getTime();

        // Continue any previous path finding that didn't finish last turn
        if (this.pathFindingStore) {
            console.error("Continuing pathfinding for (" + this.pathFindingStore.startRow + ", " + this.pathFindingStore.startCol + ")");
            var pathInfo = this.astar.plotCourseContinuation(this.pathFindingStore);
            var ant = this.pathFindingStore.ant;
            this._continueAntPathing(ant, pathInfo);
        }

        
        // Add formation locations ;)
        var formation = [];
        for (var hi = 0; hi < this.fogMap._hills.length; hi++) {
            if (this.fogMap._ants.length <= 12 * (hi + 1)) { break; }
            var hill = this.fogMap._hills[hi];
            var thisthis = this;
            this.fogMap.circle(hill.row, hill.col, 2, function(row, col) {
                var loc = thisthis.fogMap._map[row][col];
                if (loc.isPassable && formation.indexOf(loc) === -1) {
                    formation.push(loc);
                }
            });
        }

        var food = this.fogMap._food.slice();
        if (Defines.DEBUG) {
            console.error("Visible food: " + food.length);
        }

        for (var fi = 0, ti = 0; fi < this.fogMap._food.length; fi++, ti++) {
            if (this.targets.indexOf(this.fogMap._food[fi]) !== -1) {
                food.splice(ti, 1);
                ti--;
            }
        }
        var unseen = this.fogMap._lightFog.slice();
        for (var fi = 0, ti = 0; fi < this.fogMap._lightFog.length; fi++, ti++) {
            if (this.targets.indexOf(this.fogMap._lightFog[fi]) !== -1) {
                unseen.splice(ti, 1);
                ti--;
            }
        }

        if (Defines.DEBUG) {
            console.error("Narrowed food: " + food.length);
        }
        for (var ai = 0; ai < this.fogMap._ants.length; ai++) {
            var ant = this.fogMap._ants[ai];
            if (ant.path && ant.path.length > 0) {
                continue;
            }

            var goal = this.getGoal(ant, food, formation, unseen);
            if (goal) {
                this.moveToLocation(ant, goal);
            }
        }

        if (Defines.DEBUG) {
            console.error("Path time: " + (new Date().getTime() - startTime) + "ms");
        }

        
        startTime = new Date().getTime();
        this.moveAnts();
        if (Defines.DEBUG) {
            console.error("Move time: " + (new Date().getTime() - startTime) + "ms");
        }
        
        if (Defines.DEBUG) {
            console.error("Turn: " + this.fogMap._currentTurn + "|onTurn End");
        }
        ants.finishTurn();
    }

    this.onEnd = function() {
    
    }
}
ants.start(new Bot());
